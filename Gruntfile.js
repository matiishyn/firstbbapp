module.exports = function (grunt) {

    grunt.initConfig({
	
		connect: {
			server: {
				options: {
					port: 8080,
					base: './',
					keepalive: true,
					//debug: true,
					//livereload: true,
					hostname: "*" // Now you can access from 127.0.0.1 and localhost, and 192.168.137.128 (you CentOS IP)
				}
			}
	}
	
    });

	grunt.loadNpmTasks('grunt-contrib-connect');


    grunt.registerTask("default", ["connect"]);
	
}